FROM openjdk:15.0.2-jdk
EXPOSE 8080
COPY build/libs/mano-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]