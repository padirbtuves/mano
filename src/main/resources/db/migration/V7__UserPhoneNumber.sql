ALTER TABLE USERS
ADD COLUMN phone VARCHAR(50);

ALTER TABLE USERS
ADD COLUMN phone_validated INT default null;

ALTER TABLE USERS
ADD CONSTRAINT uniq_phone UNIQUE(phone, phone_validated);
