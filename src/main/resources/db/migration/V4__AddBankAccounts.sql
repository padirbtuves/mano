CREATE SEQUENCE BANK_ACCOUNTS_seq;
CREATE SEQUENCE BANK_TRANSACTIONS_seq;
CREATE SEQUENCE BANK_ACCOUNTS_LOG_seq;

CREATE TABLE BANK_ACCOUNTS (
    id INTEGER PRIMARY KEY,
    iban VARCHAR(34) not null unique,
    currency varchar(3) not null,
    description varchar(200),
    created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()
);

CREATE TABLE BANK_TRANSACTIONS (
    id INTEGER PRIMARY KEY,
    account_id integer not null,
    amount decimal not null,
    currency varchar(3) not null,
    debtor_name varchar(200),
    debtor_account varchar(34),
    creditor_name varchar(200),
    creditor_account varchar(34),
    booking_date date not null,
    entry_reference varchar(20),
    transaction_id varchar(100) not null,
    description varchar(200),
    created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
    FOREIGN KEY (account_id) REFERENCES BANK_ACCOUNTS(id),
    CONSTRAINT BANK_TRANSACTIONS_ACC_ID_TRANSACTION UNIQUE (account_id, transaction_id)
);

CREATE TABLE BANK_ACCOUNTS_LOGS (
    id INTEGER PRIMARY KEY,
    account_id integer not null,
    status varchar(20) not null,
    message varchar(500),
    created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()
)
