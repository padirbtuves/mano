ALTER TABLE USERS ADD COLUMN email_subscription boolean default false;
ALTER TABLE USERS ADD COLUMN email_validated boolean default false;

UPDATE USERS SET email_validated = true WHERE id in (SELECT user_id from GOOGLE_LOGINS);