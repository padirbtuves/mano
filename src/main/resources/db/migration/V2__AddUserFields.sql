ALTER TABLE USERS
ADD COLUMN username VARCHAR(50);

UPDATE USERS SET username = id;

ALTER TABLE USERS
ADD CONSTRAINT users_user_uniq UNIQUE(username);

ALTER TABLE USERS
alter column username set NOT NULL;

ALTER TABLE USERS
ADD COLUMN password VARCHAR(100);

UPDATE USERS SET password = '';

ALTER TABLE USERS
alter column password set NOT NULL;
