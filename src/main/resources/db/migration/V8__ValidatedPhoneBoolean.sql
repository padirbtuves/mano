ALTER TABLE USERS
DROP CONSTRAINT uniq_phone;

ALTER TABLE USERS
DROP COLUMN phone_validated;

ALTER TABLE USERS
ADD COLUMN phone_validated BOOLEAN default null;

ALTER TABLE USERS
ADD CONSTRAINT uniq_phone UNIQUE(phone, phone_validated);
