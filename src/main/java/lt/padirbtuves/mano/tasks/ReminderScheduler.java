package lt.padirbtuves.mano.tasks;

import lt.padirbtuves.mano.service.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
@Profile("prod")
public class ReminderScheduler {

    @Value("${service.email.reminder}")
    private String reminder;
    @Autowired
    private PermissionService permissionService;
    @Scheduled(cron = "0 0 11 * * ?")
    public void emailReminder() {
        permissionService.expiredPermission(30, reminder);
        permissionService.expiredPermission(1, reminder);
    }

}
