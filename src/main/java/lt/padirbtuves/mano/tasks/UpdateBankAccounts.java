package lt.padirbtuves.mano.tasks;

import lt.padirbtuves.mano.jpa.BankAccount;
import lt.padirbtuves.mano.repository.BankAccountRepository;
import lt.padirbtuves.mano.service.BankAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.stream.StreamSupport;

@Service
@Profile("prod")
public class UpdateBankAccounts {

    @Autowired
    private BankAccountRepository bankAccountRepository;

    @Autowired
    private BankAccountService bankAccountService;

//    @Scheduled(cron = "${tasks.bank.account.update.cron:0 17 23 * * ?}")
    public void updateBankAccounts() {
        bankAccountService.syncAccounts();
        StreamSupport.stream(bankAccountRepository.findAll().spliterator(), false)
                .map(BankAccount::getIban)
                .forEach(bankAccountService::syncAccountTransactions);
    }
}
