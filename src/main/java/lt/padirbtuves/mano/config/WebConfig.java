package lt.padirbtuves.mano.config;

import lt.padirbtuves.mano.repository.RoleRepository;
import lt.padirbtuves.mano.service.UserDetailsService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

import javax.sql.DataSource;


@Configuration
@EnableWebSecurity
public class WebConfig {

    @Bean
    public UserDetailsService getUserDetailsService(DataSource dataSource) {
        return new UserDetailsService(dataSource);
    }

    @Bean
    public PasswordEncoder getPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/", "/login", "/register", "/hook/**", "/api/**").permitAll()
                .antMatchers("/webjars/**/*").permitAll()
                .antMatchers("/admin/**/*").hasAnyAuthority(RoleRepository.ROLE_ADMIN)
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/login")
                .loginPage("/")
                .defaultSuccessUrl("/details")
                .permitAll()
                .and()
                .logout()
                .logoutSuccessUrl("/")
                .and()
                .oauth2Login()
                .loginPage("/login")
                .loginPage("/")
                .defaultSuccessUrl("/details")
                .and()
                .csrf()
                .disable();

        return http.build();
    }
}
