package lt.padirbtuves.mano.jpa;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.validator.internal.util.stereotypes.Lazy;

import javax.persistence.*;
import java.time.Instant;


@Entity
@Table(name = "permissions")
public class Permission {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "permission_seq")
    @SequenceGenerator(name = "permission_seq", sequenceName = "permission_seq", allocationSize = 1)
    private Long id;

    @Enumerated(EnumType.STRING)
    private PermissionName name;

    @ManyToOne
    @JoinColumn(name = "user_id")
    @Lazy
    private User user;

    private Instant created;

    private Instant starts;

    private Instant ends;

    @ManyToOne
    @JoinColumn(name = "created_by")
    @Lazy
    private User createdBy;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PermissionName getName() {
        return name;
    }

    public void setName(PermissionName name) {
        this.name = name;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Instant getCreated() {
        return created;
    }

    public void setCreated(Instant created) {
        this.created = created;
    }

    public Instant getStarts() {
        return starts;
    }

    public void setStarts(Instant starts) {
        this.starts = starts;
    }

    public Instant getEnds() {
        return ends;
    }

    public void setEnds(Instant ends) {
        this.ends = ends;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public boolean isActive() {
        return Instant.now().isAfter(getStarts()) && Instant.now().isBefore(getEnds());
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toStringExclude(this, "id", "user", "created", "createdBy");
    }

    public boolean isUpcoming() {
        return Instant.now().isBefore(getStarts());
    }

}
