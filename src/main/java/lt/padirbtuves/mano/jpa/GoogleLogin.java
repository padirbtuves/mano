package lt.padirbtuves.mano.jpa;

import javax.persistence.*;

@Entity
@Table(name = "google_logins")
public class GoogleLogin {

    @Id
    @Column(name = "user_id")
    private Long userId;

    @Column(name = "google_id", nullable = false, unique = true, length = 50)
    private String googleId;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getGoogleId() {
        return googleId;
    }

    public void setGoogleId(String googleId) {
        this.googleId = googleId;
    }
}
