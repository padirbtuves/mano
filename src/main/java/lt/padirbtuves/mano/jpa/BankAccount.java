package lt.padirbtuves.mano.jpa;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.annotations.JoinFormula;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(name = "bank_accounts")
public class BankAccount {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "bank_accounts_seq")
    @SequenceGenerator(name = "bank_accounts_seq", sequenceName = "bank_accounts_seq", allocationSize = 1)
    private Long id;

    @Column(nullable = false, length = 34, unique = true)
    private String iban;

    @Column(nullable = false, length = 3)
    private String currency;

    @Column(length = 200)
    private String description;

    private Instant created;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinFormula("(" +
            "SELECT b.id " +
            "FROM bank_accounts_logs b " +
            "WHERE b.account_id = id " +
            "ORDER BY b.created DESC " +
            "LIMIT 1" +
            ")")
    private BankAccountLog latestLog;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Instant getCreated() {
        return created;
    }

    public void setCreated(Instant created) {
        this.created = created;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    public BankAccountLog getLatestLog() {
        return latestLog;
    }
}
