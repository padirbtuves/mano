package lt.padirbtuves.mano.jpa;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.validator.internal.util.stereotypes.Lazy;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(name = "bank_accounts_logs")
public class BankAccountLog {

    public static final String ERROR = "ERROR";

    public static final String UPDATED = "UPDATED";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "bank_accounts_log_seq")
    @SequenceGenerator(name = "bank_accounts_log_seq", sequenceName = "bank_accounts_log_seq", allocationSize = 1)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "account_id")
    @Lazy
    private BankAccount bankAccount;

    @Column(length = 20)
    private String status;

    @Column(length = 500)
    private String message;

    private Instant created = Instant.now();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BankAccount getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Instant getCreated() {
        return created;
    }

    public void setCreated(Instant created) {
        this.created = created;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
