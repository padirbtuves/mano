package lt.padirbtuves.mano.controller;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Call;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.net.URI;

@Controller
@RequestMapping("/gate")
public class GateController {

    private static final Logger logger = LoggerFactory.getLogger(GateController.class);

    @Value("${service.twilio.account.sid}")
    private String ACCOUNT_SID;

    @Value("${service.twilio.account.token}")
    private String AUTH_TOKEN;

    @GetMapping("/open")
    public String syncAccounts() {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Call call = Call.creator(
                        new com.twilio.type.PhoneNumber("+37068305068"),
                        new com.twilio.type.PhoneNumber("+37052141314"),
                        URI.create("http://demo.twilio.com/docs/voice.xml"))
                .create();

        logger.info("Call to gale {}", call.getSid());

        return "redirect:/details";
    }

}
