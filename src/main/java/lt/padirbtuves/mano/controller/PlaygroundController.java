package lt.padirbtuves.mano.controller;

import lt.padirbtuves.mano.dto.play.SendEmailForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
@RequestMapping("/admin/play")
@Secured("ROLE_ADMIN")
public class PlaygroundController {

    @Autowired
    private JavaMailSender emailSender;

    @GetMapping("/email")
    public String emailForm(Model model) {
        model.addAttribute("sendEmailForm", new SendEmailForm());
        return "play/email";
    }

    @PostMapping("/email")
    public String emailPost(@Valid SendEmailForm sendEmailForm) {
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setTo(sendEmailForm.getEmail());
        simpleMailMessage.setText(sendEmailForm.getMessage());
        simpleMailMessage.setFrom("labas@padirbtuves.lt");

        emailSender.send(simpleMailMessage);

        return "play/email";
    }
}
