package lt.padirbtuves.mano.controller;

import lt.padirbtuves.mano.service.EntryService;

import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/admin/entry")
@Secured("ROLE_ADMIN")
public class EntryController {

    private final EntryService entryService;

    public EntryController(EntryService entryService) {
        this.entryService = entryService;
    }

    @GetMapping
    public String userList(Model model) {
        model.addAttribute("entryItems", entryService.getUserEntry());
        return "user/entry";
    }
}
