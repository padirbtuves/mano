package lt.padirbtuves.mano.controller;

import lt.padirbtuves.mano.dto.AccountEditForm;
import lt.padirbtuves.mano.dto.RegisterForm;
import lt.padirbtuves.mano.dto.UserInfo;
import lt.padirbtuves.mano.jpa.User;
import lt.padirbtuves.mano.repository.UserRepository;
import lt.padirbtuves.mano.service.ConfirmationTokenService;
import lt.padirbtuves.mano.service.PhoneService;
import lt.padirbtuves.mano.service.UserDetailsService;
import lt.padirbtuves.mano.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ResolvableType;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Controller
public class HomeController {

    private final Logger logger = LoggerFactory.getLogger(HomeController.class);

    private static final String AUTHORIZATION_REQUEST_BASE_URI = "oauth2/authorization";
    private static final String REGISTER_FORM = "registerForm";
    private static final String ACCOUNT_EDIT_FORM = "accountEditForm";
    private static final String REGISTER = "register";

    @Autowired
    private PhoneService phoneService;
    @Autowired
    private ConfirmationTokenService confirmationTokenService;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private ClientRegistrationRepository clientRegistrationRepository;

    @GetMapping({"/", "/login"})
    public String login(Authentication auth, Model model) {

        if (auth != null && auth.isAuthenticated()) {
            return "redirect:details";
        } else {
            Iterable<ClientRegistration> clientRegistrations = null;
            ResolvableType type = ResolvableType.forInstance(clientRegistrationRepository).as(Iterable.class);
            if (type != ResolvableType.NONE && ClientRegistration.class.isAssignableFrom(type.resolveGenerics()[0])) {
                clientRegistrations = (Iterable<ClientRegistration>) clientRegistrationRepository;
            }

            Map<String, String> oauth2AuthenticationUrls = new HashMap<>();
            if (clientRegistrations != null) {
                clientRegistrations.forEach(registration -> oauth2AuthenticationUrls.put(registration.getClientName(),
                                                                                         AUTHORIZATION_REQUEST_BASE_URI + "/" + registration.getRegistrationId()));
            }
            model.addAttribute("urls", oauth2AuthenticationUrls);
            return "login";
        }
    }

    @GetMapping("/details")
    public String details(Model model, Authentication auth) {
        UserInfo userInfo = (UserInfo) auth.getPrincipal();
        model.addAttribute("greeting", userInfo.getUsername());
        User user = getUser(auth);
        model.addAttribute("user", user);
        model.addAttribute("permissions", userService.getPermission(user));

        return "details";
    }

    private User getUser(Authentication authentication) {
        UserInfo userInfo = (UserInfo) authentication.getPrincipal();
        return userRepository.findById(userInfo.getUserId()).orElseThrow();
    }

    @GetMapping("/register")
    public String register(Model model) {
        model.addAttribute(REGISTER_FORM, new RegisterForm());
        return REGISTER;
    }

    @PostMapping("/register")
    public String registerPost(
            @Valid RegisterForm registerForm,
            BindingResult bindingResult,
            Model model,
            HttpServletRequest request) {
        if (bindingResult.hasErrors()) {
            model.addAttribute(REGISTER_FORM, registerForm);
            return REGISTER;
        }

        try {
            userDetailsService.registerNewUser(registerForm.getUsername(), registerForm.getPassword());
        } catch (DataIntegrityViolationException e) {
            bindingResult.addError(new FieldError(
                    REGISTER_FORM,
                    "username",
                    registerForm.getUsername(),
                    false,
                    new String[] {"register.username.error.exists"},
                    null,
                    "Username already exists default"));
            model.addAttribute(REGISTER_FORM, registerForm);
            return REGISTER;
        }

        try {
            request.login(registerForm.getUsername(), registerForm.getPassword());
        } catch (ServletException e) {
            logger.warn("Failed login after registration. Redirect to manual login.", e);
            return "redirect:login";
        }

        return "redirect:details";
    }

    @GetMapping("/account/edit")
    public String editAccount(Model model, Authentication authentication) {
        User user = getUser(authentication);

        AccountEditForm accountEditForm = new AccountEditForm();
        accountEditForm.setPhone(user.getPhone());
        accountEditForm.setEmail(user.getEmail());
        accountEditForm.setEmailSubscription(user.isEmailSubscription());

        model.addAttribute(ACCOUNT_EDIT_FORM, accountEditForm);

        return "account/edit";
    }

    @GetMapping("/account/verify")
    public String verifyPhone(Model model, Authentication authentication) {
        User user = getUser(authentication);

        if (!user.getPhoneValidated()) {
            phoneService.waitForVerificationCall(user.getPhone(), user.getId());
        }

        model.addAttribute("user", user);

        return "account/verify";
    }

    @PostMapping("/account/edit")
    public String editAccountPost(
            @Valid AccountEditForm accountEditForm,
            BindingResult bindingResult,
            Model model,
            Authentication authentication) {
        if (bindingResult.hasErrors()) {
            model.addAttribute(ACCOUNT_EDIT_FORM, accountEditForm);
            return "account/edit";
        }

        User user = getUser(authentication);

        if (!Objects.equals(user.getPhone(), accountEditForm.getPhone())) {
            user.setPhone(accountEditForm.getPhone());
            user.setPhoneValidated(null);
        }

        if (accountEditForm.isEmailSubscription() && accountEditForm.getEmail().isEmpty()) {
            bindingResult.addError(new FieldError(
                    ACCOUNT_EDIT_FORM,
                    "email",
                    accountEditForm.getEmail(),
                    false,
                    new String[]{"email.subscription.error"},
                    null,
                    "You cannot subscribe without email"));
            model.addAttribute(ACCOUNT_EDIT_FORM, accountEditForm);
            return "account/edit";
        }
        if (!Objects.equals(user.getEmail(), accountEditForm.getEmail())) {
            user.setEmail(accountEditForm.getEmail());
            user.setEmailValidated(false);

            if (!accountEditForm.getEmail().isEmpty()){
                confirmationTokenService.createToken(user);
            }
        }

        user.setEmailSubscription(accountEditForm.isEmailSubscription());

        try {
            userRepository.save(user);
        } catch (DataIntegrityViolationException e) {
            bindingResult.addError(new FieldError(
                    ACCOUNT_EDIT_FORM,
                    "phone",
                    accountEditForm.getPhone(),
                    false,
                    new String[] {"phone.unknown.error"},
                    null,
                    "Unknown phone error"));
            model.addAttribute(ACCOUNT_EDIT_FORM, accountEditForm);
            return "account/edit";
        }

        return "redirect:/details";
    }

}
