package lt.padirbtuves.mano.controller;

import lt.padirbtuves.mano.service.ConfirmationTokenService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/api")
public class VerificationController {

    private final ConfirmationTokenService confirmationTokenService;

    public VerificationController(ConfirmationTokenService confirmationTokenService) {
        this.confirmationTokenService = confirmationTokenService;
    }

    @GetMapping(path = "verification", produces = {"application/json"})
    public String verify(@RequestParam("token") String token, Model model) {
         model.addAttribute("message", confirmationTokenService.validateToken(token));
        return "account/verification";
    }

}
