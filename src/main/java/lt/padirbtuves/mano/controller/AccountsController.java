package lt.padirbtuves.mano.controller;

import java.util.Collections;
import java.util.List;

import lt.padirbtuves.mano.jpa.BankTransaction;
import lt.padirbtuves.mano.repository.BankAccountRepository;
import lt.padirbtuves.mano.repository.BankTransactionRepository;
import lt.padirbtuves.mano.service.BankAccountService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/admin/accounts")
public class AccountsController {

    @Autowired
    private BankAccountService bankAccountService;

    @Autowired
    private BankAccountRepository bankAccountRepository;

    @Autowired
    private BankTransactionRepository bankTransactionRepository;

    @GetMapping
    public String accounts(Model model) {
        model.addAttribute("accounts", bankAccountRepository.findAll());
        return "admin/accounts";
    }

    @PostMapping("/sync")
    public String syncAccounts() {
        bankAccountService.syncAccounts();
        return "redirect:/admin/accounts";
    }

    @GetMapping("/{iban}")
    public String account(@PathVariable("iban") String iban, Model model) {
        List<BankTransaction> transactions = bankAccountRepository
                .findByIban(iban)
                .map(bankTransactionRepository::findByBankAccountOrderByBookingDateDesc)
                .orElse(Collections.emptyList());

        model.addAttribute("transactions", transactions);
        return "admin/account";
    }

    @PostMapping("/{iban}/sync")
    public String syncAccount(@PathVariable("iban") String iban) {
        bankAccountService.syncAccountTransactions(iban);
        return "redirect:/admin/accounts/" + iban;
    }



}
