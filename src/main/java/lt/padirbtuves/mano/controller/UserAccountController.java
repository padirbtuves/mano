package lt.padirbtuves.mano.controller;

import javax.validation.Valid;

import lt.padirbtuves.mano.dto.EditPermissionForm;
import lt.padirbtuves.mano.jpa.PermissionName;
import lt.padirbtuves.mano.jpa.User;
import lt.padirbtuves.mano.service.PermissionService;
import lt.padirbtuves.mano.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/admin/users")
@Secured("ROLE_ADMIN")
public class UserAccountController {

    @Autowired
    private PermissionService permissionService;

    private final UserService userService;

    public UserAccountController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public String userList(Model model) {
        model.addAttribute("users", userService.getUsers());
        return "user/users";
    }

    @GetMapping("/{userId}/permissions")
    public String userPermissions(Model model, @PathVariable(name = "userId") long userId) {
        model.addAttribute("user", userService.getUser(userId));
        return "user/permissions";
    }

    @DeleteMapping("/{userId}/permissions/{permissionId}")
    public String deleteUserPermissions(
            Model model,
            @PathVariable(name = "userId") long userId,
            @PathVariable(name = "permissionId") long permissionId) {

        permissionService.deletePermission(userId, permissionId);

        User user = userService.getUser(userId);
        model.addAttribute("user", user);

        return "redirect:/admin/users/%d/permissions".formatted(user.getId());
    }

    @PostMapping("/{userId}/permissions/{permissionId}")
    public String saveUserPermissions(
            @Valid EditPermissionForm editPermissionForm,
            Model model,
            @PathVariable(name = "userId") long userId,
            @PathVariable(name = "permissionId") long permissionId) {

        permissionService.updatePermission(userId, permissionId, editPermissionForm.getStarts(),
                                           editPermissionForm.getEnds());

        User user = userService.getUser(userId);
        model.addAttribute("user", user);

        return "redirect:/admin/users/%d/permissions".formatted(user.getId());
    }

    @PostMapping("/extend/{userId}/{permission}")
    public String extendPermissionForUser(
            @PathVariable(name = "userId") long userId,
            @PathVariable(name = "permission") String permission) {
        PermissionName permissionName = PermissionName.valueOf(permission);
        permissionService.createPermissionFor(permissionName, userId);

        User user = userService.getUser(userId);

        return "redirect:/admin/users/%d/permissions".formatted(user.getId());
    }
}
