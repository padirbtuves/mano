package lt.padirbtuves.mano.service;

import lt.padirbtuves.mano.dto.LoginCode;
import lt.padirbtuves.mano.dto.Phone;
import org.springframework.stereotype.Service;

import javax.validation.Valid;

@Service
public class SmsService {

    public void send(Phone phone, @Valid LoginCode code) {
        System.out.println("Sending " + code.getValue() + " to " + phone.getValue());
    }
}
