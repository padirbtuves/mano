package lt.padirbtuves.mano.service;


import lt.padirbtuves.mano.jpa.User;
import org.apache.commons.collections4.map.PassiveExpiringMap;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.util.Collections;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Service
public class ConfirmationTokenService {

    private final UserService userService;
    private final EmailSenderService emailSenderService;
    /**
     * Key is token, value is user id
     */
    private final Map<String, Long> emailVerificationTokens = Collections.synchronizedMap(new PassiveExpiringMap<>(20, TimeUnit.MINUTES));
    @Value("${service.link:test}")
    private String link;
    @Value("${service.email.verification:test}")
    private String verification;

    public ConfirmationTokenService(UserService userService,
                                    EmailSenderService emailSenderService) {
        this.userService = userService;
        this.emailSenderService = emailSenderService;
    }

    public void createToken(User user) {
        String token = UUID.randomUUID().toString();

        emailVerificationTokens.put(token, user.getId());
        try {
            String tokenLink = link + token;
            emailSenderService.send(user.getEmail(), tokenLink, verification);
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

    public String validateToken(String token) {
        Long userId = emailVerificationTokens.remove(token);
        if (userId == null) {
            return "Token not found!";
        }

        User user = userService.getUser(userId);
        if (user != null) {
            userService.setUserEmailValidated(user);
            return "Your email is confirmed. Thank you for using our service!";
        } else {
            return "No user found for this token";
        }
    }
}
