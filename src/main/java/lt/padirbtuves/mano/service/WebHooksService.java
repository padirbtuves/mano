package lt.padirbtuves.mano.service;

import lt.padirbtuves.mano.dto.LockResponse;
import lt.padirbtuves.mano.jpa.PermissionName;
import lt.padirbtuves.mano.jpa.User;
import lt.padirbtuves.mano.repository.PermissionRepository;
import lt.padirbtuves.mano.repository.UserRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.twilio.twiml.VoiceResponse;
import com.twilio.twiml.voice.Reject;

@Service
public class WebHooksService {

    private static final Logger logger = LoggerFactory.getLogger(WebHooksService.class);

    @Value("${service.twilio.account.sid}")
    private String accountSid;

    @Autowired
    private PhoneService phoneService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PermissionRepository permissionRepository;

    @Autowired
    private EntryService entryService;

    private final LockService lockService;

    public WebHooksService(LockService lockService) {
        this.lockService = lockService;
    }

    public VoiceResponse getVoiceResponse(String number, String sid) {
        Reject.Reason reason = Reject.Reason.REJECTED;

        logger.info("Incoming call from {}", number);
        if (accountSid.equals(sid)) {

            // First check if there is number waiting for verification
            if (phoneService.acceptVerificationCall(number)) {
                logger.info("User phone verified {}", number);
                reason = Reject.Reason.BUSY;
            } else {
                // No user is waiting to verity this number, try to open door
                reason = userRepository.findByPhoneAndPhoneValidatedTrue(number)
                                       .filter(this::checkPermissions)
                                       .map(
                                               user -> {
                                                   logger.info("Unlock door for user {}", user);
                                                   lockService.unlock();
                                                   entryService.createEntryLog(user);
                                                   return Reject.Reason.BUSY;
                                               })
                                       .orElseGet(() -> {
                                           logger.info("No suitable user or permissions found");
                                           return Reject.Reason.REJECTED;
                                       });
            }
        } else {
            logger.warn("Incorrect account SID {}", sid);
        }
        logger.info("Sending response with reason {}", reason);

        Reject reject = new Reject.Builder().reason(reason).build();
        return new VoiceResponse.Builder().reject(reject).build();
    }

    public LockResponse getLockedResponse() {
        LockResponse response = new LockResponse();
        response.setLocked(lockService.isLocked());
        return response;
    }

    private boolean checkPermissions(User user) {
        return user.getPermissions()
                   .stream()
                   .anyMatch(permission -> PermissionName.ENTRANCE.equals(permission.getName()) && permission.isActive());
    }
}
