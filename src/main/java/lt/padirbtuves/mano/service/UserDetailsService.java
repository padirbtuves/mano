package lt.padirbtuves.mano.service;

import lt.padirbtuves.mano.dto.UserInfo;
import lt.padirbtuves.mano.jpa.PasswordLogin;
import lt.padirbtuves.mano.jpa.Role;
import lt.padirbtuves.mano.jpa.User;
import lt.padirbtuves.mano.repository.PasswordLoginRepository;
import lt.padirbtuves.mano.repository.RoleRepository;
import lt.padirbtuves.mano.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.jdbc.JdbcDaoImpl;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.sql.DataSource;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

public class UserDetailsService extends JdbcDaoImpl {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordLoginRepository passwordLoginRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public UserDetailsService(DataSource dataSource) {
        super();

        setUsersByUsernameQuery("""
                select pl.username, pl.password, true as enabled
                from users u
                join password_logins pl on pl.user_id = u.id
                where pl.username = ?
                """);

        setAuthoritiesByUsernameQuery("""
                select pl.username, r.name as authority
                from users u
                join password_logins pl on pl.user_id = u.id
                join users_roles ur on u.id = ur.user_id
                join roles r on r.id = ur.role_id
                where pl.username = ?
                """);
        setEnableGroups(false);

        setDataSource(dataSource);
    }

    protected UserDetails createUserDetails(String username, UserDetails userFromUserQuery,
                                            List<GrantedAuthority> combinedAuthorities) {
        UserInfo.FormUserInfo result = new UserInfo.FormUserInfo(
                userFromUserQuery.getUsername(),
                userFromUserQuery.getPassword(),
                userFromUserQuery.isEnabled(),
                userFromUserQuery.isAccountNonExpired(),
                userFromUserQuery.isCredentialsNonExpired(),
                userFromUserQuery.isAccountNonLocked(),
                combinedAuthorities);
        result.setUserId(passwordLoginRepository.findByUsername(username).orElseThrow(RuntimeException::new).getUserId());

        return result;
    }

    @Transactional
    public void registerNewUser(String username, String password) {
        List<Role> roles = new ArrayList<>();
        roles.add(roleRepository.findByName(RoleRepository.ROLE_USER).orElseThrow(RuntimeException::new));
        if (userRepository.count() == 0) {
            roles.add(roleRepository.findByName(RoleRepository.ROLE_ADMIN).orElseThrow(RuntimeException::new));
        }

        User newUser = new User();
        newUser.setRoles(roles);
        newUser.setName(username);
        newUser = userRepository.save(newUser);

        PasswordLogin passwordLogin = new PasswordLogin();
        passwordLogin.setUsername(username);
        passwordLogin.setPassword(passwordEncoder.encode(password));
        passwordLogin.setUserId(newUser.getId());

        passwordLoginRepository.save(passwordLogin);

    }
}
