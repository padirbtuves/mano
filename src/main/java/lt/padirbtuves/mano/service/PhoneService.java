package lt.padirbtuves.mano.service;

import lt.padirbtuves.mano.repository.UserRepository;
import org.apache.commons.collections4.map.PassiveExpiringMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Service
public class PhoneService {

    @Autowired
    private UserRepository userRepository;

    private final Map<String, Long> phoneToUserId = Collections.synchronizedMap(new PassiveExpiringMap<>(20, TimeUnit.SECONDS));

    /**
     * Adds user phone and id to wait list for 20 seconds
     *
     * @param phone  User phone
     * @param userId User id
     */
    public void waitForVerificationCall(String phone, Long userId) {
        phoneToUserId.put(phone, userId);
    }

    /**
     * Accept verification call from phone number. If there is a user waiting for verification
     * from this number, mark it as verified and return true else return false. If there is
     * another user with same phone number which is verified, reset verification.
     *
     * @param phone Phone number from which verification call is incoming
     * @return If user vas verified
     */
    public boolean acceptVerificationCall(String phone) {
        boolean result = false;

        Long userId = phoneToUserId.remove(phone);
        if (userId != null) {
            result = userRepository.findById(userId).map(u -> {

                // For other user with this phone number mark phone as unverified
                userRepository
                        .findByPhoneAndPhoneValidatedTrue(phone)
                        .ifPresent(otherUser -> {
                            otherUser.setPhoneValidated(null);
                            userRepository.save(otherUser);
                        });

                u.setPhoneValidated(true);
                userRepository.save(u);

                return true;
            }).orElse(false);
        }

        return result;
    }
}
