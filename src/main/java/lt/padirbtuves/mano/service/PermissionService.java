package lt.padirbtuves.mano.service;

import lt.padirbtuves.mano.dto.ExpiredUserPermission;
import lt.padirbtuves.mano.dto.UserInfo;
import lt.padirbtuves.mano.jpa.Permission;
import lt.padirbtuves.mano.jpa.PermissionName;
import lt.padirbtuves.mano.jpa.User;
import lt.padirbtuves.mano.repository.CustomRepository;
import lt.padirbtuves.mano.repository.PermissionRepository;
import lt.padirbtuves.mano.repository.UserRepository;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class PermissionService {

    private static final ZoneId ZONE_ID = ZoneId.of("GMT");

    private final PermissionRepository permissionRepository;
    private final EmailSenderService emailSenderService;
    private final CustomRepository customRepository;
    private final UserRepository userRepository;

    public PermissionService(PermissionRepository permissionRepository,
                             EmailSenderService emailSenderService,
                             CustomRepository customRepository,
                             UserRepository userRepository) {
        this.permissionRepository = permissionRepository;
        this.emailSenderService = emailSenderService;
        this.customRepository = customRepository;
        this.userRepository = userRepository;
    }


    @Secured("ROLE_ADMIN")
    public void createPermissionFor(PermissionName permissionName, long targetUserId) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserInfo userInfo = (UserInfo) auth.getPrincipal();

        User user = userRepository.findById(userInfo.getUserId()).orElseThrow();
        User targetUser = userRepository.findById(targetUserId).orElseThrow();

        Instant instant = targetUser.getPermissions().stream().filter(permission -> permissionName.equals(permission.getName()) && (permission.isActive() || permission.isUpcoming())).map(Permission::getEnds).max(Instant::compareTo).orElse(Instant.now());
        Permission permission = new Permission();
        permission.setCreatedBy(user);
        permission.setCreated(Instant.now());
        permission.setUser(targetUser);
        permission.setName(permissionName);
        permission.setStarts(instant);
        permission.setEnds(Instant.from(LocalDate.ofInstant(instant, ZONE_ID).plusDays(1).plusMonths(6).atStartOfDay().atZone(ZONE_ID)));

        permissionRepository.save(permission);
    }

    public void deletePermission(long userId, long permissionId) {
        permissionRepository.deleteById(permissionId);
    }

    public void updatePermission(long userId, long permissionId, Instant starts, Instant ends) {
        permissionRepository.findById(permissionId).map(p -> {
            p.setStarts(starts);
            p.setEnds(ends);
            return permissionRepository.save(p);
        });
    }

    public void expiredPermission(int days, String template) {
        List<ExpiredUserPermission> expiredUserPermissions = customRepository.getExpiredPermissionStats(days);
        Map<Long, List<ExpiredUserPermission>> listMap = expiredUserPermissions.stream().collect(Collectors.groupingBy(ExpiredUserPermission::getId));
        listMap.forEach((key, value) -> {
            try {
                emailSenderService.send(value, template);
            } catch (MessagingException e) {
                throw new RuntimeException(e);
            }
        });
    }

}
