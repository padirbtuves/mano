package lt.padirbtuves.mano.service;

import lt.padirbtuves.mano.dto.EntryItem;
import lt.padirbtuves.mano.jpa.Entry;
import lt.padirbtuves.mano.jpa.User;
import lt.padirbtuves.mano.repository.CustomRepository;
import lt.padirbtuves.mano.repository.EntryRepository;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;

@Service
public class EntryService {

    private final EntryRepository entryRepository;

    private final CustomRepository customRepository;

    public EntryService(EntryRepository entryRepository, CustomRepository customRepository) {
        this.entryRepository = entryRepository;
        this.customRepository = customRepository;
    }

    public void createEntryLog(User user) {
        Entry entry = new Entry();
        entry.setUser(user);
        entry.setCreated(Instant.now());
        entryRepository.save(entry);
    }

    public List<EntryItem> getUserEntry() {
        return customRepository.getUserEntryStats();
    }
}
