package lt.padirbtuves.mano.service;

import lt.padirbtuves.mano.dto.ExpiredUserPermission;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class EmailSenderService {
    private static final String LOGO = "emails/logo.png";
    private static final String PNG_MIME = "image/png";
    private final JavaMailSender emailSender;
    @Value("${service.email.from:test}")
    private String emailFrom;

    @Value("${service.email.subject:test}")
    private String emailSubject;

    public EmailSenderService(JavaMailSender emailSender) {
        this.emailSender = emailSender;
    }


    public void send(List<ExpiredUserPermission> expiredUserPermissions, String template) throws MessagingException {
        Map<String, Object> properties = new HashMap<>();
        properties.put("expired", expiredUserPermissions);
        send(expiredUserPermissions.get(0).getEmail(), properties, template);
    }

    public void send(String email, String link, String template) throws MessagingException {
        Map<String, Object> properties = new HashMap<>();
        properties.put("link", link);
        send(email, properties, template);
    }

    private void send(String email, Map<String, Object> properties, String template) throws MessagingException {
        MimeMessage message = emailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message,
                MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                StandardCharsets.UTF_8.name());
        Context context = new Context();
        context.setVariables(properties);
        context.setVariable("logo", "logo");
        helper.setTo(email);
        helper.setFrom(emailFrom);
        helper.setSubject(emailSubject);
        String html = getTemplateEngine().process(template, context);
        helper.setText(html, true);
        helper.addInline("logo", new ClassPathResource(LOGO), PNG_MIME);
        emailSender.send(message);

    }

    private SpringTemplateEngine getTemplateEngine(){
        SpringTemplateEngine springTemplateEngine = new SpringTemplateEngine();
        springTemplateEngine.addTemplateResolver(fileTemplateResolver());
        return springTemplateEngine;
    }
    private ClassLoaderTemplateResolver fileTemplateResolver() {
        ClassLoaderTemplateResolver fileTemplateResolver = new ClassLoaderTemplateResolver();
        fileTemplateResolver.setPrefix("emails/");
        fileTemplateResolver.setSuffix(".html");
        fileTemplateResolver.setTemplateMode(TemplateMode.HTML);
        fileTemplateResolver.setCharacterEncoding(StandardCharsets.UTF_8.name());
        fileTemplateResolver.setCacheable(false);
        return fileTemplateResolver;
    }
}
