package lt.padirbtuves.mano.service;

import lt.padirbtuves.mano.dto.nordigen.NordingenAccountDetailsResponse;
import lt.padirbtuves.mano.jpa.BankAccount;
import lt.padirbtuves.mano.jpa.BankAccountLog;
import lt.padirbtuves.mano.jpa.BankTransaction;
import lt.padirbtuves.mano.repository.BankAccountLogRepository;
import lt.padirbtuves.mano.repository.BankAccountRepository;
import lt.padirbtuves.mano.repository.BankTransactionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;

@Service
public class BankAccountService {

    private static final Logger logger = LoggerFactory.getLogger(BankAccountService.class);

    @Autowired
    private BankAccountRepository bankAccountRepository;

    @Autowired
    private BankTransactionRepository bankTransactionRepository;

    @Autowired
    private BankAccountLogRepository bankAccountLogRepository;

    @Autowired
    private NordigenService nordigenService;

    /**
     * Looks for all available bank accounts from external systems and creates records in bank accounts table
     * if they are not present there
     */
    @Transactional
    public void syncAccounts() {
        nordigenService.getRequisitions().getResults().stream().collect(
                        ArrayList<String>::new,
                        (acc, requisition) -> acc.addAll(requisition.getAccounts()),
                        ArrayList::addAll
                ).stream().map(nordigenService::getAccountDetails).map(NordingenAccountDetailsResponse::getAccount)
                .forEach(acc -> {
                    if (bankAccountRepository.findByIban(acc.getIban()).isEmpty()) {
                        BankAccount newBankAccount = new BankAccount();
                        newBankAccount.setCurrency(acc.getCurrency());
                        newBankAccount.setDescription(acc.getName());
                        newBankAccount.setIban(acc.getIban());
                        newBankAccount.setCreated(Instant.now());

                        bankAccountRepository.save(newBankAccount);
                    }
                });
    }

    private void logBankAccountStatus(BankAccount bankAccount, String status) {
        logBankAccountStatus(bankAccount, status, null);
    }

    private void logBankAccountStatus(BankAccount bankAccount, String status, String message) {
        BankAccountLog log = new BankAccountLog();
        log.setBankAccount(bankAccount);
        log.setStatus(status);
        log.setMessage(message);
        bankAccountLogRepository.save(log);
    }

    /**
     * Try to get new transactions from account information providers
     *
     * @param iban Account IBAN
     */
    public void syncAccountTransactions(String iban) {
        DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE.withZone(ZoneId.of("UTC"));

        bankAccountRepository.findByIban(iban).ifPresent(bankAccount -> {
            try {
                nordigenService.getAccountId(iban).ifPresent(accountId -> {
                    String lastBookingDate = bankTransactionRepository
                            .findFirstByBankAccountOrderByBookingDateDesc(bankAccount)
                            .map(BankTransaction::getBookingDate)
                            .map(Date::toInstant)
                            .map(formatter::format)
                            .orElse("2020-01-01");

                    nordigenService.getAccountTransactions(accountId, lastBookingDate)
                            .getTransactions()
                            .getBooked()
                            .forEach(t -> {
                                BankTransaction transaction = new BankTransaction();
                                transaction.setBankAccount(bankAccount);

                                transaction.setAmount(t.getTransactionAmount().getAmount());
                                transaction.setCurrency(t.getTransactionAmount().getCurrency());

                                transaction.setCreditorName(t.getCreditorName());
                                t.getCreditorAccount().ifPresent(a -> transaction.setCreditorAccount(a.getIban()));

                                transaction.setDebtorName(t.getDebtorName());
                                t.getDebtorAccount().ifPresent(a -> transaction.setDebtorAccount(a.getIban()));

                                transaction.setBookingDate(Date.from(LocalDate.parse(t.getBookingDate(), formatter).atStartOfDay().toInstant(ZoneOffset.UTC)));
                                transaction.setCreated(Instant.now());
                                transaction.setDescription(t.getRemittanceInformationUnstructured());
                                transaction.setEntryReference(t.getEntryReference());
                                transaction.setTransactionId(t.getTransactionId());

                                try {
                                    bankTransactionRepository.save(transaction);
                                } catch (DataIntegrityViolationException dive) {
                                    // Unique constraint violated (transactionId + accountId)
                                    // Transaction already present. Ignore
                                }
                            });
                    logBankAccountStatus(bankAccount, BankAccountLog.UPDATED);
                });
            } catch (Exception e) {
                logger.error("Failed synchronising " + bankAccount, e);
                logBankAccountStatus(bankAccount, BankAccountLog.ERROR, e.getMessage());
            }
        });
    }
}
