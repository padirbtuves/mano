package lt.padirbtuves.mano.service;

import lt.padirbtuves.mano.dto.nordigen.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Service
public class NordigenService {

    @Value("${service.nordigen.secret.id}")
    private String secretId;

    @Value("${service.nordigen.secret.key}")
    private String secretKey;

    @Autowired
    @Qualifier("nordigenRestTemplate")
    private RestTemplate restTemplate;

    private NordigenTokenReponse refreshToken(String refreshToken) {
        return restTemplate.postForObject("/api/v2/token/refresh/",
                new HashMap<String, String>() {{
                    put("refresh", refreshToken);
                }}, NordigenTokenReponse.class);
    }

    private NordigenTokenReponse getAccessToken() {
        return restTemplate.postForObject("/api/v2/token/new/",
                new HashMap<String, String>() {{
                    put("secret_id", secretId);
                    put("secret_key", secretKey);
                }}, NordigenTokenReponse.class);
    }

    public NordingenRequisitionsResponse getRequisitions() {
        ResponseEntity<NordingenRequisitionsResponse> response =
                restTemplate.exchange("/api/v2/requisitions/", HttpMethod.GET, getEntity(), NordingenRequisitionsResponse.class);

        // check response status code
        return response.getBody();
    }

    public NordingenAccountDetailsResponse getAccountDetails(String accountId) {
        ResponseEntity<NordingenAccountDetailsResponse> response = restTemplate.exchange(
                "/api/v2/accounts/{accountId}/details",
                HttpMethod.GET,
                getEntity(),
                NordingenAccountDetailsResponse.class, accountId);

        // check response status code
        return response.getBody();
    }

    public NordingenAccountTransactionsResponse getAccountTransactions(String accountId, String since) {
        ResponseEntity<NordingenAccountTransactionsResponse> response = restTemplate.exchange(
                "/api/v2/accounts/{accountId}/transactions?date_from={since}",
                HttpMethod.GET,
                getEntity(),
                NordingenAccountTransactionsResponse.class, accountId, since);

        // check response status code
        return response.getBody();
    }

    public NordingenAgreementsResponse getAgreements() {
        ResponseEntity<NordingenAgreementsResponse> response =
                restTemplate.exchange(
                        "/api/v2/agreements/enduser/",
                        HttpMethod.GET,
                        getEntity(),
                        NordingenAgreementsResponse.class);

        return response.getBody();
    }

    public NordingenAgreement createAgreement(NordingenInstitution institution) {
        NordingenAgreementRequest agreementRequest = new NordingenAgreementRequest();
        agreementRequest.setInstitutionId(institution.getId());
        agreementRequest.setMaxHistoricalDays(institution.getTransactionTotalDays());

        ResponseEntity<NordingenAgreement> response =
                restTemplate.exchange("/api/v2/agreements/enduser/", HttpMethod.POST, getEntity(agreementRequest), NordingenAgreement.class);

        return response.getBody();
    }


    public List<NordingenInstitution> getInstitutions() {
        ResponseEntity<List<NordingenInstitution>> response =
                restTemplate.exchange(
                        "/api/v2/institutions/?country=lt",
                        HttpMethod.GET,
                        getEntity(),
                        new ParameterizedTypeReference<List<NordingenInstitution>>() {
                        });

        return response.getBody();
    }

    private String getToken() {
        return getAccessToken().getAccess();
    }

    private <T> HttpEntity<T> getEntity(T content) {
        final HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + getToken());

        return new HttpEntity<>(content, headers);
    }

    private <T> HttpEntity<T> getEntity() {
        return getEntity(null);
    }

    public NordingenAccountResponse getAccount(String accountId) {
        ResponseEntity<NordingenAccountResponse> response = restTemplate.exchange(
                "/api/v2/accounts/{accountId}/",
                HttpMethod.GET,
                getEntity(),
                NordingenAccountResponse.class, accountId);

        // check response status code
        return response.getBody();
    }

    /**
     * Checks all requisite nordingen accounts to se if any of them matches specified iban and returns nordingen
     * account id
     *
     * @param iban nordingen account id
     */
    public Optional<String> getAccountId(String iban) {
        return getRequisitions()
                .getResults()
                .stream()
                .map(NordingenRequisition::getAccounts)
                .flatMap(List::stream)
                .map(this::getAccount)
                .filter(a -> a.getIban().equals(iban))
                .map(NordingenAccountResponse::getId)
                .findFirst();
    }
}
