package lt.padirbtuves.mano.service;

import lt.padirbtuves.mano.jpa.Permission;
import lt.padirbtuves.mano.jpa.PermissionName;
import lt.padirbtuves.mano.jpa.User;
import lt.padirbtuves.mano.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
public class UserService {

    public static final Predicate<Permission> ACTIVE = input -> Instant.now().isBefore(input.getEnds());

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<User> getUsers() {
        List<User> result = userRepository.findAll();

        result.sort((Comparator<? super User>) (a, b) -> {
            Function<User, Instant> getSubscriptionEndInstant = (u) ->
                    u.getPermissions()
                            .stream()
                            .filter(p -> p.getName().equals(PermissionName.ENTRANCE))
                            .map(Permission::getEnds)
                            .min(Comparator.reverseOrder())
                            .orElse(null);

            return Objects.compare(
                    getSubscriptionEndInstant.apply(a),
                    getSubscriptionEndInstant.apply(b),
                    Comparator.nullsLast(Comparator.reverseOrder()));
        });

        return result;
    }

    public User getUser(long userId) {
        return userRepository.findById(userId).orElseThrow();
    }

    public List<Permission> getPermission(User user) {
        List<Permission> permissions;
        List<Permission> permissionList = user.getPermissions();
        permissions = permissionList.stream().filter(ACTIVE).collect(Collectors.toList());
        if (permissionExist(permissionList, permissions, PermissionName.ENTRANCE)) {
            getPermission(permissionList, PermissionName.ENTRANCE).ifPresent(permissions::add);
        }
        if (permissionExist(permissionList, permissions, PermissionName.BOX)) {
            getPermission(permissionList, PermissionName.BOX).ifPresent(permissions::add);
        }

        return permissions;
    }

    private boolean permissionExist(
            List<Permission> permissionList,
            List<Permission> permissions,
            PermissionName name) {
        return permissions.stream().noneMatch(input -> input.getName().equals(name)) &&
                permissionList.stream().filter(ACTIVE.negate()).anyMatch(input -> input.getName().equals(name));
    }

    private Optional<Permission> getPermission(List<Permission> permissionList, PermissionName name) {
        return permissionList.stream()
                .filter(input -> input.getName().equals(name))
                .max(Comparator.comparing(Permission::getId));
    }


    public void setUserEmailValidated(User user) {
        user.setEmailValidated(true);
        userRepository.save(user);
    }

}
