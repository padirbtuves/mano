package lt.padirbtuves.mano.service;

import lt.padirbtuves.mano.dto.UserInfo;
import lt.padirbtuves.mano.jpa.GoogleLogin;
import lt.padirbtuves.mano.jpa.Role;
import lt.padirbtuves.mano.jpa.User;
import lt.padirbtuves.mano.repository.GoogleLoginRepository;
import lt.padirbtuves.mano.repository.RoleRepository;
import lt.padirbtuves.mano.repository.UserRepository;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.client.userinfo.DefaultOAuth2UserService;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequest;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class OAuth2UserService implements org.springframework.security.oauth2.client.userinfo.OAuth2UserService<OAuth2UserRequest, OAuth2User> {

    @Autowired
    private UserService userService;

    @Autowired
    private GoogleLoginRepository googleLoginRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;


    @Override
    @Transactional
    public OAuth2User loadUser(OAuth2UserRequest userRequest) throws OAuth2AuthenticationException {
        org.springframework.security.oauth2.client.userinfo.OAuth2UserService<OAuth2UserRequest, OAuth2User> oAuth2UserService = new DefaultOAuth2UserService();
        OAuth2User oAuth2User = oAuth2UserService.loadUser(userRequest);
        String sub = oAuth2User.getAttribute("sub");
        String email = oAuth2User.getAttribute("email");
        String name = oAuth2User.getAttribute("name");

        User user = googleLoginRepository
                .findByGoogleId(sub)
                .map(googleLogin -> userRepository
                        .findById(googleLogin.getUserId())
                        .orElseThrow(RuntimeException::new))
                .orElseGet(() -> {
                    List<Role> roles = new ArrayList<>();
                    roles.add(roleRepository.findByName(RoleRepository.ROLE_USER).orElseThrow(RuntimeException::new));
                    if (userRepository.count() == 0) {
                        roles.add(roleRepository.findByName(RoleRepository.ROLE_ADMIN).orElseThrow(RuntimeException::new));
                    }

                    User newUser = new User();
                    newUser.setRoles(roles);
                    if (StringUtils.isNotBlank(email)) {
                        newUser.setEmail(email);
                        newUser.setEmailValidated(true);
                    }
                    if (StringUtils.isNotBlank(name)) {
                        newUser.setName(name);
                    }
                    newUser = userRepository.save(newUser);

                    GoogleLogin googleLogin = new GoogleLogin();
                    googleLogin.setGoogleId(sub);
                    googleLogin.setUserId(newUser.getId());
                    googleLoginRepository.save(googleLogin);

                    return newUser;
                });

        // Update user email and name if changed
        if (ObjectUtils.notEqual(user.getEmail(), email)) {
            user.setEmail(email);
            user = userRepository.save(user);
        }

        if (ObjectUtils.notEqual(user.getName(), name)) {
            user.setEmail(email);
            user = userRepository.save(user);
        }

        Set<GrantedAuthority> mappedAuthorities = user
                .getRoles()
                .stream()
                .map(Role::getName)
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toSet());

        UserInfo.OAuth2UserInfo result = new UserInfo.OAuth2UserInfo(mappedAuthorities, oAuth2User.getAttributes(), "name");
        result.setUserId(user.getId());

        return result;
    }
}
