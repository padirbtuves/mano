package lt.padirbtuves.mano.repository;

import lt.padirbtuves.mano.jpa.Role;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface RoleRepository extends CrudRepository<Role, Long> {

    String ROLE_ADMIN = "ADMIN";

    String ROLE_USER = "USER";

    Optional<Role> findByName(String id);
}
