package lt.padirbtuves.mano.repository;

import lt.padirbtuves.mano.jpa.PasswordLogin;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface PasswordLoginRepository extends CrudRepository<PasswordLogin, Long> {

    Optional<PasswordLogin> findByUsername(String username);
}
