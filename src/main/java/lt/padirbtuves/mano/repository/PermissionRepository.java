package lt.padirbtuves.mano.repository;

import lt.padirbtuves.mano.jpa.Permission;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PermissionRepository extends CrudRepository<Permission, Long> {

    @Query("SELECT p FROM Permission p WHERE p.user.id = :userId AND p.starts < CURRENT_TIMESTAMP AND p.ends > CURRENT_TIMESTAMP")
    List<Permission> getActivePermissionsForUser(@Param("userId") long userId);

}
