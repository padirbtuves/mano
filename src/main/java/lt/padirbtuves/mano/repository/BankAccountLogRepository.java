package lt.padirbtuves.mano.repository;

import lt.padirbtuves.mano.jpa.BankAccount;
import lt.padirbtuves.mano.jpa.BankAccountLog;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface BankAccountLogRepository extends CrudRepository<BankAccountLog, Long> {

    Optional<BankAccountLog> findTopByBankAccountOrderByCreatedDesc(BankAccount bankAccount);
}
