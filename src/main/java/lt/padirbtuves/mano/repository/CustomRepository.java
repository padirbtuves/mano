package lt.padirbtuves.mano.repository;

import lt.padirbtuves.mano.dto.EntryItem;
import lt.padirbtuves.mano.dto.ExpiredUserPermission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;

@Service
public class CustomRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<EntryItem> getUserEntryStats() {
        return jdbcTemplate.query("""
                select u.name, u.email, u.phone, count(distinct CAST(e.created AS DATE)) as count, max(e.created) as last_entry
                from entry e
                join users u on e.user_id = u.id
                where e.created > ?
                group by u.id
                order by max(e.created) desc
                """, BeanPropertyRowMapper.newInstance(EntryItem.class), Date.from(Instant.now().atZone(ZoneId.of("GMT")).minus(6, ChronoUnit.MONTHS).toInstant()));
    }

    /**
     * Find permissions that expire in coming number of days. Searches only users who have email subscriptions.
     *
     * @param expiresIn number of days after which permission expires
     * @return Returns permissions with user information about expiring permissions
     */

    public List<ExpiredUserPermission> getExpiredPermissionStats(int expiresIn) {
        return jdbcTemplate.query("""
                select  u.id, 
                        u.email, 
                        p.name as type,                     
                        p.ends as date 
                from permissions p
                inner join
                   (select pr.user_id, 
                           pr.name as name, 
                           max(pr.ends) as date
                   from permissions pr
                   group by pr.name, pr.user_id) as item on item.name = p.name and item.date = p.ends 
               join users u on p.user_id = u.id
               where p.ends  between ? and ? and u.email_validated = true and u.email_subscription = true
               group by u.id, u.email, p.name, p.ends
                """,
                BeanPropertyRowMapper.newInstance(ExpiredUserPermission.class),
                Date.from(Instant
                        .now()
                        .atZone(ZoneId.of("GMT"))
                        .plus(expiresIn, ChronoUnit.DAYS)
                        .toInstant()),
                Date.from(Instant
                        .now()
                        .atZone(ZoneId.of("GMT"))
                        .plus(expiresIn, ChronoUnit.DAYS)
                        .plus(1, ChronoUnit.DAYS)
                        .toInstant())
                );
    }
}
