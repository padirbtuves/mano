package lt.padirbtuves.mano.repository;

import lt.padirbtuves.mano.jpa.BankAccount;
import lt.padirbtuves.mano.jpa.BankTransaction;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface BankTransactionRepository extends CrudRepository<BankTransaction, Long> {

    Optional<BankTransaction> findFirstByBankAccountOrderByBookingDateDesc(BankAccount bankAccount);

    List<BankTransaction> findByBankAccountOrderByBookingDateDesc(BankAccount bankAccount);
}
