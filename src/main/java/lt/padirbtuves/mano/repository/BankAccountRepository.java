package lt.padirbtuves.mano.repository;

import lt.padirbtuves.mano.jpa.BankAccount;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface BankAccountRepository extends CrudRepository<BankAccount, Long> {

    Optional<BankAccount> findByIban(String iban);
}
