package lt.padirbtuves.mano.repository;

import lt.padirbtuves.mano.jpa.Entry;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface EntryRepository extends CrudRepository<Entry, Long> {

    List<Entry> findAll();

}
