package lt.padirbtuves.mano.repository;

import java.util.List;
import java.util.Optional;

import lt.padirbtuves.mano.jpa.User;

import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {

    Optional<User> findByPhoneAndPhoneValidatedTrue(String number);

    List<User> findAll();

}
