package lt.padirbtuves.mano.repository;

import lt.padirbtuves.mano.jpa.GoogleLogin;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface GoogleLoginRepository extends CrudRepository<GoogleLogin, Long> {

    Optional<GoogleLogin> findByGoogleId(String googleId);
}
