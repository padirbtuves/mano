package lt.padirbtuves.mano.dto;

import java.time.Instant;

public class EntryItem {

    private String name;
    private String email;
    private String phone;
    private long count;

    private Instant lastEntry;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public Instant getLastEntry() {
        return lastEntry;
    }

    public void setLastEntry(Instant lastEntry) {
        this.lastEntry = lastEntry;
    }
}
