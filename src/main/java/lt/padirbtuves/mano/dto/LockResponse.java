package lt.padirbtuves.mano.dto;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class LockResponse {

	private boolean locked;

	public boolean isLocked() {
		return locked;
	}

	public void setLocked(boolean locked) {
		this.locked = locked;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
