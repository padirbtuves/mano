package lt.padirbtuves.mano.dto.converter;

import org.springframework.core.convert.converter.Converter;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;

public class StringToInstant implements Converter<String, Instant> {

    @Override
    public Instant convert(String source) {
        return Instant.from(LocalDate.parse(source).atStartOfDay(ZoneId.of("GMT")));
    }
}
