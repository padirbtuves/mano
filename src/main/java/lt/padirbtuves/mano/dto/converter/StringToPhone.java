package lt.padirbtuves.mano.dto.converter;

import lt.padirbtuves.mano.dto.Phone;
import org.springframework.core.convert.converter.Converter;

public class StringToPhone implements Converter<String, Phone> {

    @Override
    public Phone convert(String source) {
        return new Phone(source);
    }
}
