package lt.padirbtuves.mano.dto.converter;

import lt.padirbtuves.mano.dto.LoginCode;
import org.springframework.core.convert.converter.Converter;

public class StringToCode implements Converter<String, LoginCode> {
    @Override
    public LoginCode convert(String source) {
        return new LoginCode(source);
    }
}
