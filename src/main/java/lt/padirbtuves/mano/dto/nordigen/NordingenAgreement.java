package lt.padirbtuves.mano.dto.nordigen;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.Instant;
import java.util.List;

public class NordingenAgreement {

    private String id;

    private Instant created;

    @JsonProperty("max_historical_days")
    private Integer maxHistoricalDays;

    @JsonProperty("access_valid_for_days")
    private Integer accessValidForDays;

    @JsonProperty("access_scope")
    private List<String> accessScope;

    private Instant accepted;

    @JsonProperty("institution_id")
    private String institutionId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Instant getCreated() {
        return created;
    }

    public void setCreated(Instant created) {
        this.created = created;
    }

    public Integer getMaxHistoricalDays() {
        return maxHistoricalDays;
    }

    public void setMaxHistoricalDays(Integer maxHistoricalDays) {
        this.maxHistoricalDays = maxHistoricalDays;
    }

    public Integer getAccessValidForDays() {
        return accessValidForDays;
    }

    public void setAccessValidForDays(Integer accessValidForDays) {
        this.accessValidForDays = accessValidForDays;
    }

    public List<String> getAccessScope() {
        return accessScope;
    }

    public void setAccessScope(List<String> accessScope) {
        this.accessScope = accessScope;
    }

    public Instant getAccepted() {
        return accepted;
    }

    public void setAccepted(Instant accepted) {
        this.accepted = accepted;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }
}
