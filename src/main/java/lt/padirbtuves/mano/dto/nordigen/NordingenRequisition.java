package lt.padirbtuves.mano.dto.nordigen;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.Instant;
import java.util.List;

public class NordingenRequisition {

    private String id;

    private Instant created;

    private String redirect;

    private String status;

    @JsonProperty("institution_id")
    private String institutionId;

    private String agreement;

    private String reference;

    private List<String> accounts;

    @JsonProperty("user_language")
    private String userLanguage;

    private String link;

    private String ssn;

    @JsonProperty("account_selection")
    private Boolean accountSelection;

    @JsonProperty("redirect_immediate")
    private Boolean redirectImmediate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Instant getCreated() {
        return created;
    }

    public void setCreated(Instant created) {
        this.created = created;
    }

    public String getRedirect() {
        return redirect;
    }

    public void setRedirect(String redirect) {
        this.redirect = redirect;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public String getAgreement() {
        return agreement;
    }

    public void setAgreement(String agreement) {
        this.agreement = agreement;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public List<String> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<String> accounts) {
        this.accounts = accounts;
    }

    public String getUserLanguage() {
        return userLanguage;
    }

    public void setUserLanguage(String userLanguage) {
        this.userLanguage = userLanguage;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getSsn() {
        return ssn;
    }

    public void setSsn(String ssn) {
        this.ssn = ssn;
    }

    public Boolean getAccountSelection() {
        return accountSelection;
    }

    public void setAccountSelection(Boolean accountSelection) {
        this.accountSelection = accountSelection;
    }

    public Boolean getRedirectImmediate() {
        return redirectImmediate;
    }

    public void setRedirectImmediate(Boolean redirectImmediate) {
        this.redirectImmediate = redirectImmediate;
    }
}
