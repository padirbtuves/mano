package lt.padirbtuves.mano.dto.nordigen;

import java.util.List;

public class NordingenAgreementsResponse extends NordingenPaginetedResult {

    private List<NordingenAgreement> result;

    public List<NordingenAgreement> getResult() {
        return result;
    }

    public void setResult(List<NordingenAgreement> result) {
        this.result = result;
    }
}
