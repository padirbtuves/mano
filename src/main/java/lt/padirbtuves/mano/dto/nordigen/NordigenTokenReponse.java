package lt.padirbtuves.mano.dto.nordigen;

import com.fasterxml.jackson.annotation.JsonProperty;

public class NordigenTokenReponse {

    private String access;

    @JsonProperty("access_expires")
    private Integer accessExpires;

    private String refresh;

    @JsonProperty("refresh_expires")
    private Integer refreshExpires;

    public String getAccess() {
        return access;
    }

    public void setAccess(String access) {
        this.access = access;
    }

    public Integer getAccessExpires() {
        return accessExpires;
    }

    public void setAccessExpires(Integer accessExpires) {
        this.accessExpires = accessExpires;
    }

    public String getRefresh() {
        return refresh;
    }

    public void setRefresh(String refresh) {
        this.refresh = refresh;
    }

    public Integer getRefreshExpires() {
        return refreshExpires;
    }

    public void setRefreshExpires(Integer refreshExpires) {
        this.refreshExpires = refreshExpires;
    }
}
