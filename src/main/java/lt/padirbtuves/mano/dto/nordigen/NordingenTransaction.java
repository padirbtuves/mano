package lt.padirbtuves.mano.dto.nordigen;

import java.util.Optional;

public class NordingenTransaction {

    private String bankTransactionCode;

    private String bookingDate;

    private Optional<NordingenTransactionAccount> creditorAccount = Optional.empty();

    private String creditorName;

    private Optional<NordingenTransactionAccount> debtorAccount = Optional.empty();

    private String debtorName;

    private String endToEndId;

    private String entryReference;

    private String remittanceInformationUnstructured;

    private NordingenTransactionAmount transactionAmount;

    private String transactionId;

    private String valueDate;

    public String getBankTransactionCode() {
        return bankTransactionCode;
    }

    public void setBankTransactionCode(String bankTransactionCode) {
        this.bankTransactionCode = bankTransactionCode;
    }

    public String getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
    }

    public String getCreditorName() {
        return creditorName;
    }

    public void setCreditorName(String creditorName) {
        this.creditorName = creditorName;
    }

    public Optional<NordingenTransactionAccount> getCreditorAccount() {
        return creditorAccount;
    }

    public void setCreditorAccount(Optional<NordingenTransactionAccount> creditorAccount) {
        this.creditorAccount = creditorAccount;
    }

    public Optional<NordingenTransactionAccount> getDebtorAccount() {
        return debtorAccount;
    }

    public void setDebtorAccount(Optional<NordingenTransactionAccount> debtorAccount) {
        this.debtorAccount = debtorAccount;
    }

    public String getDebtorName() {
        return debtorName;
    }

    public void setDebtorName(String debtorName) {
        this.debtorName = debtorName;
    }

    public String getEndToEndId() {
        return endToEndId;
    }

    public void setEndToEndId(String endToEndId) {
        this.endToEndId = endToEndId;
    }

    public String getEntryReference() {
        return entryReference;
    }

    public void setEntryReference(String entryReference) {
        this.entryReference = entryReference;
    }

    public String getRemittanceInformationUnstructured() {
        return remittanceInformationUnstructured;
    }

    public void setRemittanceInformationUnstructured(String remittanceInformationUnstructured) {
        this.remittanceInformationUnstructured = remittanceInformationUnstructured;
    }

    public NordingenTransactionAmount getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(NordingenTransactionAmount transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getValueDate() {
        return valueDate;
    }

    public void setValueDate(String valueDate) {
        this.valueDate = valueDate;
    }

}
