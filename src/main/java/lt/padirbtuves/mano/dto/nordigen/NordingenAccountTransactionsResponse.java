package lt.padirbtuves.mano.dto.nordigen;

public class NordingenAccountTransactionsResponse {

    private NordingenTransactions transactions;

    public NordingenTransactions getTransactions() {
        return transactions;
    }

    public void setTransactions(NordingenTransactions transactions) {
        this.transactions = transactions;
    }
}
