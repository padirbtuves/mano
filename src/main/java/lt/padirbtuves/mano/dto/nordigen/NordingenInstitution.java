package lt.padirbtuves.mano.dto.nordigen;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class NordingenInstitution {

    private String id;

    private String name;

    private String bic;

    @JsonProperty("transaction_total_days")
    private Integer transactionTotalDays;

    private List<String> countries;

    private String logo;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBic() {
        return bic;
    }

    public void setBic(String bic) {
        this.bic = bic;
    }

    public Integer getTransactionTotalDays() {
        return transactionTotalDays;
    }

    public void setTransactionTotalDays(Integer transactionTotalDays) {
        this.transactionTotalDays = transactionTotalDays;
    }

    public List<String> getCountries() {
        return countries;
    }

    public void setCountries(List<String> countries) {
        this.countries = countries;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }
}
