package lt.padirbtuves.mano.dto.nordigen;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.Instant;

public class NordingenAccountResponse {

    private String id;

    private Instant created;

    @JsonProperty("last_accessed")
    private Instant lastAccessed;

    private String iban;

    @JsonProperty("institution_id")
    private String institutionId;

    private String status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Instant getCreated() {
        return created;
    }

    public void setCreated(Instant created) {
        this.created = created;
    }

    public Instant getLastAccessed() {
        return lastAccessed;
    }

    public void setLastAccessed(Instant lastAccessed) {
        this.lastAccessed = lastAccessed;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
