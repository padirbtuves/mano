package lt.padirbtuves.mano.dto.nordigen;

public class NordingenAccountDetailsResponse {

    private NordingenAccountDetails account;

    public NordingenAccountDetails getAccount() {
        return account;
    }

    public void setAccount(NordingenAccountDetails account) {
        this.account = account;
    }
}
