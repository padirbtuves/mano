package lt.padirbtuves.mano.dto.nordigen;

public class NordingenTransactionAccount {

    private String iban;

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }
}
