package lt.padirbtuves.mano.dto.nordigen;

import java.util.List;

public class NordingenRequisitionsResponse extends NordingenPaginetedResult {

    private List<NordingenRequisition> results;

    public List<NordingenRequisition> getResults() {
        return results;
    }

    public void setResults(List<NordingenRequisition> results) {
        this.results = results;
    }
}
