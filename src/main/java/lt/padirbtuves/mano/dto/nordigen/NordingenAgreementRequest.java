package lt.padirbtuves.mano.dto.nordigen;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class NordingenAgreementRequest {

    @JsonProperty("institution_id")
    private String institutionId;

    @JsonProperty("max_historical_days")
    private Integer maxHistoricalDays;

    @JsonProperty("access_valid_for_days")
    private Integer accessValidForDays;

    @JsonProperty("access_scope")
    private List<String> accessScope;

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public Integer getMaxHistoricalDays() {
        return maxHistoricalDays;
    }

    public void setMaxHistoricalDays(Integer maxHistoricalDays) {
        this.maxHistoricalDays = maxHistoricalDays;
    }

    public Integer getAccessValidForDays() {
        return accessValidForDays;
    }

    public void setAccessValidForDays(Integer accessValidForDays) {
        this.accessValidForDays = accessValidForDays;
    }

    public List<String> getAccessScope() {
        return accessScope;
    }

    public void setAccessScope(List<String> accessScope) {
        this.accessScope = accessScope;
    }
}
