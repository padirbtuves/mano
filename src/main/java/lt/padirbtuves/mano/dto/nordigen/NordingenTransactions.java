package lt.padirbtuves.mano.dto.nordigen;

import java.util.List;

public class NordingenTransactions {

    private List<NordingenTransaction> booked;

    private List<NordingenTransaction> pending;

    public List<NordingenTransaction> getBooked() {
        return booked;
    }

    public void setBooked(List<NordingenTransaction> booked) {
        this.booked = booked;
    }

    public List<NordingenTransaction> getPending() {
        return pending;
    }

    public void setPending(List<NordingenTransaction> pending) {
        this.pending = pending;
    }
}
