package lt.padirbtuves.mano.dto.play;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

public class SendEmailForm {

    @Email
    private String email;

    @NotBlank
    private String message;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
