package lt.padirbtuves.mano.dto;

public class ExpiredUserPermission {

    private Long id;
    private String email;
    private String type;
    private String date;

    public ExpiredUserPermission() {
    }


    public ExpiredUserPermission(Long id, String email, String type, String date) {
        this.id = id;
        this.email = email;
        this.type = type;
        this.date = date;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
