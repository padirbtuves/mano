package lt.padirbtuves.mano.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;

public class AccountEditForm {

    @Pattern(regexp = "\\+\\d+", message = "{phone.edit.constraint.pattern}")
    private String phone;
    @Email
    private String email;
    private boolean emailSubscription;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public boolean isEmailSubscription() {
        return emailSubscription;
    }

    public void setEmailSubscription(boolean emailSubscription) {
        this.emailSubscription = emailSubscription;
    }
}
