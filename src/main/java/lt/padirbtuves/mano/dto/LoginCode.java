package lt.padirbtuves.mano.dto;

public class LoginCode {

    private String value;

    public LoginCode(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
