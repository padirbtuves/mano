package lt.padirbtuves.mano.dto;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.oauth2.core.user.DefaultOAuth2User;

import java.util.Collection;
import java.util.Map;

public interface UserInfo {

    String getUsername();

    Long getUserId();

    class OAuth2UserInfo extends DefaultOAuth2User implements UserInfo {

        private Long userId;

        /**
         * Constructs a {@code DefaultOAuth2User} using the provided parameters.
         *
         * @param authorities      the authorities granted to the user
         * @param attributes       the attributes about the user
         * @param nameAttributeKey the key used to access the user's &quot;name&quot; from
         *                         {@link #getAttributes()}
         */
        public OAuth2UserInfo(Collection<? extends GrantedAuthority> authorities, Map<String, Object> attributes, String nameAttributeKey) {
            super(authorities, attributes, nameAttributeKey);
        }

        @Override
        public String getUsername() {
            return super.getName();
        }

        @Override
        public Long getUserId() {
            return userId;
        }

        public void setUserId(Long userId) {
            this.userId = userId;
        }
    }

    class FormUserInfo extends User implements UserInfo {

        private Long userId;
        public FormUserInfo(String username, String password, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities) {
            super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
        }

        @Override
        public Long getUserId() {
            return userId;
        }

        public void setUserId(Long userId) {
            this.userId = userId;
        }
    }
}


