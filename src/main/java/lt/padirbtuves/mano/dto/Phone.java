package lt.padirbtuves.mano.dto;


import javax.validation.constraints.Pattern;

public class Phone {

    @Pattern(regexp = "\\+\\d+")
    private String value;

    public Phone(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
