package lt.padirbtuves.mano.dto;

import lt.padirbtuves.mano.anot.FieldsValueMatch;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@FieldsValueMatch.List({
        @FieldsValueMatch(
                field = "password",
                fieldMatch = "passwordAgain",
                message = "Passwords do not match!"
        )
})
public class RegisterForm {

    @Pattern(regexp = "[a-z0-9]+", message = "{register.username.constraint.pattern}")
    private String username;

    @Size(min = 6, message = "{register.password.constraint.size}")
    private String password;

    private String passwordAgain;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordAgain() {
        return passwordAgain;
    }

    public void setPasswordAgain(String passwordAgain) {
        this.passwordAgain = passwordAgain;
    }
}
