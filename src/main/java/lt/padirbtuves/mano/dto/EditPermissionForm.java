package lt.padirbtuves.mano.dto;

import java.time.Instant;

public class EditPermissionForm {

    private Instant starts;

    private Instant ends;

    public Instant getStarts() {
        return starts;
    }

    public void setStarts(Instant starts) {
        this.starts = starts;
    }

    public Instant getEnds() {
        return ends;
    }

    public void setEnds(Instant ends) {
        this.ends = ends;
    }
}
