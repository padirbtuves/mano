package lt.padirbtuves.mano.rest;

import lt.padirbtuves.mano.service.DoorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.thymeleaf.extras.springsecurity5.auth.Authorization;

@RestController()
@RequestMapping("/api/door/")

public class DoorController {

    @Autowired
    private DoorService doorService;

    @GetMapping("open")
    public ResponseEntity<Void> openDoor(Authorization auth) {
        //Object obj = auth.getAuthentication().getPrincipal();
        doorService.open();

        return ResponseEntity.ok(null);
    }
}
