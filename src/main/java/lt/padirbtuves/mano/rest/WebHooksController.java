package lt.padirbtuves.mano.rest;

import com.twilio.twiml.VoiceResponse;
import lt.padirbtuves.mano.dto.LockResponse;
import lt.padirbtuves.mano.service.WebHooksService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/hook")
public class WebHooksController {

    private final WebHooksService webHooksService;

    public WebHooksController(WebHooksService webHooksService) {
        this.webHooksService = webHooksService;
    }

    @PostMapping(path = "twilio", produces = {"application/xml", "text/xml"})
    public ResponseEntity<String> twilioIncomingCall(
            @RequestParam(name = "From") String from, @RequestParam(name = "AccountSid") String sid) {

        VoiceResponse response = webHooksService.getVoiceResponse(from, sid);
        return new ResponseEntity<>(response.toXml(), HttpStatus.OK);
    }

    @GetMapping(path = "lock", produces = {"application/json"})
    public @ResponseBody LockResponse getLockStatus() {
        return webHooksService.getLockedResponse();
    }

}
