package lt.padirbtuves.mano.rest;

import lt.padirbtuves.mano.dto.UserInfo;
import lt.padirbtuves.mano.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.thymeleaf.extras.springsecurity5.auth.Authorization;

@RestController
@RequestMapping("/api/users/")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("me")
    public UserInfo me(Authorization auth) {
        return null;
    }
}
