package lt.padirbtuves.mano.repository;

import lt.padirbtuves.mano.dto.ExpiredUserPermission;
import lt.padirbtuves.mano.jpa.PermissionName;
import lt.padirbtuves.mano.jpa.Role;
import lt.padirbtuves.mano.jpa.User;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static lt.padirbtuves.mano.sample.PermissionSample.getPermission;
import static org.junit.jupiter.api.Assertions.assertEquals;
@SpringBootTest
class CustomRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PermissionRepository permissionRepository;

    @Autowired
    private CustomRepository customRepository;

    @AfterEach
    void removePermissions(){
        permissionRepository.deleteAll();
    }

    @Test
    void foundTwoExpiredPermission() {
        Role role = roleRepository.findByName(RoleRepository.ROLE_USER).orElseThrow();
        User user = new User();
        user.setEmail("email");
        user.setEmailSubscription(true);
        user.setEmailValidated(true);
        user.setRoles(List.of(role));
        user = userRepository.save(user);

        User user2 = new User();
        user2.setEmail("email2");
        user2.setEmailSubscription(true);
        user2.setEmailValidated(true);
        user2.setRoles(List.of(role));
        user2 = userRepository.save(user2);

        permissionRepository.save(getPermission(user, PermissionName.BOX, 10, 31));
        permissionRepository.save(getPermission(user, PermissionName.ENTRANCE, 10, 31));
        permissionRepository.save(getPermission(user2, PermissionName.ENTRANCE, 10, 32));
        List<ExpiredUserPermission> expiredUserPermissions = customRepository.getExpiredPermissionStats(30);
        assertEquals(2L, expiredUserPermissions.size());
    }

    @Test
    void notFoundexpiredPermissionBeforeMonth() {
        Role role = roleRepository.findByName(RoleRepository.ROLE_USER).orElseThrow();
        User user = new User();
        user.setEmail("email");
        user.setEmailSubscription(true);
        user.setEmailValidated(true);
        user.setRoles(List.of(role));
        user = userRepository.save(user);

        permissionRepository.save(getPermission(user, PermissionName.BOX, 10, 20));
        permissionRepository.save(getPermission(user, PermissionName.ENTRANCE, 10, 32));

        List<ExpiredUserPermission> expiredUserPermissions = customRepository.getExpiredPermissionStats(30);
        assertEquals(0L, expiredUserPermissions.size());
    }
    @Test
    void notFoundExpiredPermission() {
        permissionRepository.deleteAll();
        Role role = roleRepository.findByName(RoleRepository.ROLE_USER).orElseThrow();
        User user = new User();
        user.setEmail("email");
        user.setEmailValidated(true);
        user.setEmailSubscription(true);
        user.setRoles(List.of(role));
        user = userRepository.save(user);

        permissionRepository.save(getPermission(user, PermissionName.BOX, 10, 60));

        List<ExpiredUserPermission> expiredUserPermissions = customRepository.getExpiredPermissionStats(30);
        assertEquals(0, expiredUserPermissions.size());

    }

    @Test
    void noSubscriptionExistExpiredPermission() {
        Role role = roleRepository.findByName(RoleRepository.ROLE_USER).orElseThrow();
        User user = new User();
        user.setEmail("email");
        user.setEmailValidated(true);
        user.setEmailSubscription(false);
        user.setRoles(List.of(role));
        user = userRepository.save(user);

        permissionRepository.save(getPermission(user, PermissionName.BOX, 10, 30));

        List<ExpiredUserPermission> expiredUserPermissions = customRepository.getExpiredPermissionStats(30);
        assertEquals(0, expiredUserPermissions.size());
    }
}