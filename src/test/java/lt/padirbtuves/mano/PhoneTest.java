package lt.padirbtuves.mano;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.*;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.htmlunit.MockMvcWebClientBuilder;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
class PhoneTest {

    @Autowired
    WebApplicationContext context;

    WebClient webClient;

    @BeforeEach
    public void setup() {
        webClient = MockMvcWebClientBuilder
                .webAppContextSetup(context, SecurityMockMvcConfigurers.springSecurity())
                .build();

        webClient.getOptions().setJavaScriptEnabled(false);
    }

    @AfterEach
    public void close() {
        webClient.close();
    }

    @BeforeAll
    public void createUserAndLogin() throws IOException {
        setup();

        HtmlPage registerFormPage = webClient.getPage("http://localhost/register");

        HtmlTextInput username = registerFormPage.getHtmlElementById("username");
        HtmlPasswordInput password = registerFormPage.getHtmlElementById("password");
        HtmlPasswordInput passwordAgain = registerFormPage.getHtmlElementById("passwordAgain");

        username.setText("username");
        password.setText("password");
        passwordAgain.setText("password");

        HtmlForm registerForm = registerFormPage.getFormByName("register");
        HtmlButton registerButton = registerForm.getOneHtmlElementByAttribute("button", "type", "submit");

        HtmlPage registerResult = registerButton.click();
    }

    private static void login(WebClient webClient, String username, String password) throws IOException {
        HtmlPage loginFormPage = webClient.getPage("http://localhost/login");

        HtmlTextInput usernameInput = loginFormPage.getHtmlElementById("username");
        HtmlPasswordInput passwordInput = loginFormPage.getHtmlElementById("password");

        usernameInput.setText(username);
        passwordInput.setText(password);

        HtmlForm loginForm = loginFormPage.getFormByName("login");
        HtmlButton loginButton = loginForm.getOneHtmlElementByAttribute("button", "type", "submit");

        HtmlPage loginResult = loginButton.click();
        assertEquals("/details", loginResult.getBaseURL().getPath());
    }

    @Test
    void testUpdatePhone() throws IOException {
        String phoneNumber = "+37012312345";

        login(webClient, "username", "password");

        HtmlPage detailsPage = webClient.getPage("http://localhost/details");

        HtmlElement phoneHtmlElement = detailsPage.getHtmlElementById("phone");
        assertEquals("Phone: No phone specified.", phoneHtmlElement.getVisibleText());

        HtmlAnchor setupOneHere = detailsPage.getAnchorByText("Account settings");
        HtmlPage editPhonePage = setupOneHere.click();

        assertEquals("/account/edit", editPhonePage.getBaseURL().getPath());

        HtmlForm phoneEditForm = editPhonePage.getFormByName("edit");
        HtmlTextInput phoneInput = editPhonePage.getHtmlElementById("phone");
        phoneInput.setText(phoneNumber);
        HtmlButton submit = phoneEditForm.getOneHtmlElementByAttribute("button", "type", "submit");

        detailsPage = submit.click();
        phoneHtmlElement = detailsPage.getHtmlElementById("phone");

        assertTrue(phoneHtmlElement.getVisibleText().contains(phoneNumber));
    }
}
