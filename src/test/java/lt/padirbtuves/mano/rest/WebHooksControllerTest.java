package lt.padirbtuves.mano.rest;

import java.net.URI;

import lt.padirbtuves.mano.service.WebHooksService;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.twilio.twiml.VoiceResponse;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class WebHooksControllerTest {
    @Autowired
    private TestRestTemplate restTemplate;

    @LocalServerPort
    int randomServerPort;

    @MockBean
    private WebHooksService service;

    @Test
    void callWithCorrect() throws Exception {
        when(service.getVoiceResponse("a", "a")).thenReturn(new VoiceResponse.Builder().build());
        final String baseUrl = "http://localhost:" + randomServerPort + "/hook/twilio?From=a&AccountSid=a";
        URI uri = new URI(baseUrl);
        ResponseEntity<VoiceResponse> result = this.restTemplate.exchange(uri,
                                                                          HttpMethod.POST,
                                                                          new HttpEntity<>(new HttpHeaders()),
                                                                          VoiceResponse.class);
        assertEquals(HttpStatus.OK, result.getStatusCode());

    }
}
