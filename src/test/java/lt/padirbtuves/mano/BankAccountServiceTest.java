package lt.padirbtuves.mano;

import lt.padirbtuves.mano.repository.BankAccountLogRepository;
import lt.padirbtuves.mano.repository.BankAccountRepository;
import lt.padirbtuves.mano.repository.BankTransactionRepository;
import lt.padirbtuves.mano.service.BankAccountService;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@SpringBootTest
public class BankAccountServiceTest {

    private static final Logger logger = LoggerFactory.getLogger(BankAccountService.class);

    @Autowired
    private BankAccountService bankAccountService;

    @Autowired
    private BankAccountRepository bankAccountRepository;

    @Autowired
    private BankAccountLogRepository bankAccountLogRepository;

    @Autowired
    private BankTransactionRepository bankTransactionRepository;

    @Test
    public void testSyncAccounts() {
//        String bookingDate = DateTimeFormatter.ISO_LOCAL_DATE.format(Instant.now().atZone(ZoneId.of("GMT")));
//        Date.from(LocalDate.parse("2020-01-01", DateTimeFormatter.ISO_LOCAL_DATE).atStartOfDay().toInstant(ZoneOffset.UTC));
//        bankAccountService.syncAccounts();
//        bankAccountRepository.findAll().forEach(System.out::println);
//
//
//        bankAccountRepository.findAll().forEach(bankAccount -> {
//            bankAccountService.syncAccountTransactions(bankAccount.getIban());
//            System.out.println(bankAccountLogRepository.findTopByBankAccountOrderByCreatedDesc(bankAccount));
//        });
//
//
//
//        System.out.println(StreamSupport.stream(bankTransactionRepository.findAll().spliterator(), false).count());
//
//        System.out.println("Valio");
    }
}
