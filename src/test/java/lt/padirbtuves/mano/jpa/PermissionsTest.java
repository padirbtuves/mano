package lt.padirbtuves.mano.jpa;

import lt.padirbtuves.mano.repository.PermissionRepository;
import lt.padirbtuves.mano.repository.RoleRepository;
import lt.padirbtuves.mano.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static lt.padirbtuves.mano.sample.PermissionSample.getPermission;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest()
class PermissionsTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PermissionRepository permissionRepository;

    @Test
    void testBasicPermission() {
        Role role = roleRepository.findByName(RoleRepository.ROLE_USER).orElseThrow();
        User user = new User();
        user.setRoles(List.of(role));
        user = userRepository.save(user);

        permissionRepository.save(getPermission(user, PermissionName.BOX, 30, 120 ));
        List<Permission> activePermissionsForUser = permissionRepository.getActivePermissionsForUser(user.getId());

        assertEquals(1, activePermissionsForUser.size());
        assertEquals(PermissionName.BOX, activePermissionsForUser.get(0).getName());
    }


    @Test
    void testExpiredPermission() {
        Role role = roleRepository.findByName(RoleRepository.ROLE_USER).orElseThrow();
        User user = new User();
        user.setEmail("email");
        user.setRoles(List.of(role));
        user = userRepository.save(user);

        permissionRepository.save(getPermission(user, PermissionName.BOX, 10, 20));
        permissionRepository.save(getPermission(user, PermissionName.ENTRANCE, 10, 20));

        List<Permission> activePermissionsForUser = permissionRepository.getActivePermissionsForUser(user.getId());

        assertEquals(2, activePermissionsForUser.size());
        assertEquals(PermissionName.BOX, activePermissionsForUser.get(0).getName());
    }


}
