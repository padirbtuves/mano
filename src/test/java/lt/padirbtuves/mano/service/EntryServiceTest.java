package lt.padirbtuves.mano.service;


import lt.padirbtuves.mano.dto.EntryItem;
import lt.padirbtuves.mano.jpa.User;
import lt.padirbtuves.mano.repository.CustomRepository;
import lt.padirbtuves.mano.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class EntryServiceTest {

    @Autowired
    private CustomRepository customRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EntryService entryService;

    @Test
    void getUserEntryStats() {
        User user = new User();
        user.setEmail("email.example.com");
        user.setName("example");
        user.setPhone("+12312312345");
        user = userRepository.save(user);

        // Two entries on same day should count as one. We're counting number of days user was visiting maker space.
        entryService.createEntryLog(user);
        entryService.createEntryLog(user);

        List<EntryItem> entryStat = customRepository.getUserEntryStats();
        assertEquals(1, entryStat.size());
        assertEquals("example", entryStat.get(0).getName());
        assertEquals(1, entryStat.get(0).getCount());
    }
}