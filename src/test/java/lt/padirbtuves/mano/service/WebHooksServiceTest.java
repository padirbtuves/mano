package lt.padirbtuves.mano.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@SpringBootTest
class WebHooksServiceTest {
    @MockBean
    private LockService service;

    @Autowired
    private WebHooksService webHooksService;

    @Test
    void statusLocked() {
        when(service.isLocked()).thenReturn(true);
        assertTrue(webHooksService.getLockedResponse().isLocked());
    }

    @Test
    void statusUnlocked() {
        when(service.isLocked()).thenReturn(false);
        assertFalse(webHooksService.getLockedResponse().isLocked());
    }

}
