package lt.padirbtuves.mano.service;

import java.util.List;

import lt.padirbtuves.mano.jpa.Permission;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static lt.padirbtuves.mano.sample.UserSample.getUser;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class UserServiceTest {

    @Autowired
    private UserService userService;

    @Test
    void getActivePermission() {
        List<Permission> permissions = userService.getPermission(getUser(true));
        assertEquals(2, permissions.size());
        assertTrue(permissions.get(0).isActive());
    }

    @Test
    void getNotActivePermission() {
        List<Permission> permissions = userService.getPermission(getUser(false));
        assertEquals(2, permissions.size());
        assertFalse(permissions.get(0).isActive());
    }
}