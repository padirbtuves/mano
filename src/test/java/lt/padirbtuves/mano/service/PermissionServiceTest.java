package lt.padirbtuves.mano.service;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.mail.MessagingException;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.*;

@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class PermissionServiceTest {
    @Test
    public void whenSendEmailVerified() throws MessagingException {
        EmailSenderService emailSenderService = mock(EmailSenderService.class);
        doNothing().when(emailSenderService).send(isA(List.class),isA(String.class));
        emailSenderService.send(Collections.emptyList(), "test");

        verify(emailSenderService, times(1)).send(Collections.emptyList(), "test");
    }
}