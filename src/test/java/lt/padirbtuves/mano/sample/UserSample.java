package lt.padirbtuves.mano.sample;

import lt.padirbtuves.mano.jpa.User;

import static lt.padirbtuves.mano.sample.PermissionSample.getPermissions;

public class UserSample {

    public static User getUser(boolean active){
        User user = new User();
        user.setId(1L);
        user.setName("testas");
        user.setEmail("testas");
        user.setPhone("123456");
        user.setPermissions(getPermissions(active));
        return user;
    }
}
