package lt.padirbtuves.mano.sample;

import lt.padirbtuves.mano.jpa.Permission;
import lt.padirbtuves.mano.jpa.PermissionName;
import lt.padirbtuves.mano.jpa.User;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

public class PermissionSample {

    public static List<Permission> getPermissions(boolean active) {
        List<Permission> permissionList = new ArrayList<>();
        if (active) {
            permissionList.add(getPermission(1, PermissionName.ENTRANCE, 2, 2));
            permissionList.add(getPermission(2, PermissionName.BOX, 2, 2));
        } else {
            permissionList.add(getPermission(1, PermissionName.ENTRANCE, -2, 3));
            permissionList.add(getPermission(2, PermissionName.BOX, -2, 3));
        }
        return permissionList;
    }

    public static Permission getPermission(long id, PermissionName name, long startDate, long endDate) {
        Permission permission = new Permission();
        permission.setId(id);
        permission.setName(name);
        permission.setCreated(Instant.now());
        permission.setStarts(Instant.now().minus(startDate, ChronoUnit.DAYS));
        permission.setEnds(Instant.now().plus(endDate, ChronoUnit.DAYS));
        return permission;

    }

    public static Permission getPermission(User user, PermissionName permissionName, long startDate, long endDate) {
        Permission permission = new Permission();
        permission.setCreated(Instant.now());
        permission.setStarts(Instant.now().minus(startDate, ChronoUnit.DAYS));
        permission.setEnds(Instant.now().plus(endDate, ChronoUnit.DAYS));
        permission.setName(permissionName);
        permission.setUser(user);
        permission.setCreatedBy(user);
        return permission;
    }

}
