package lt.padirbtuves.mano;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.htmlunit.MockMvcWebClientBuilder;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest()
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
public class RegisterTest {

    @Autowired
    WebApplicationContext context;

    WebClient webClient;

    @BeforeEach
    public void setup() {
        webClient = MockMvcWebClientBuilder
                .webAppContextSetup(context, SecurityMockMvcConfigurers.springSecurity())
                .build();

        webClient.getOptions().setJavaScriptEnabled(false);
    }

    @AfterEach
    public void close() {
        webClient.close();
    }

    @Test
    public void testRegistration() throws IOException {
        HtmlPage registerFormPage = webClient.getPage("http://localhost/register");

        HtmlTextInput username = registerFormPage.getHtmlElementById("username");
        HtmlPasswordInput password = registerFormPage.getHtmlElementById("password");
        HtmlPasswordInput passwordAgain = registerFormPage.getHtmlElementById("passwordAgain");

        username.setText("username");
        password.setText("password");
        passwordAgain.setText("password");

        HtmlForm registerForm = registerFormPage.getFormByName("register");
        HtmlButton registerButton = registerForm.getOneHtmlElementByAttribute("button", "type", "submit");

        HtmlPage registerResult = registerButton.click();
        assertEquals("/details", registerResult.getBaseURL().getPath());
    }

    @Test
    public void testRegistrationInvalidUsername() throws IOException {
        HtmlPage registerFormPage = webClient.getPage("http://localhost/register");

        HtmlTextInput username = registerFormPage.getHtmlElementById("username");
        HtmlPasswordInput password = registerFormPage.getHtmlElementById("password");
        HtmlPasswordInput passwordAgain = registerFormPage.getHtmlElementById("passwordAgain");

        username.setText("Username");
        password.setText("password");
        passwordAgain.setText("password");

        HtmlForm registerForm = registerFormPage.getFormByName("register");
        HtmlButton registerButton = registerForm.getOneHtmlElementByAttribute("button", "type", "submit");

        HtmlPage registerResult = registerButton.click();
        assertEquals("/register", registerResult.getBaseURL().getPath());
    }
}
