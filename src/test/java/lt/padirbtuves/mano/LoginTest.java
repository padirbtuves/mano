package lt.padirbtuves.mano;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.web.servlet.htmlunit.MockMvcWebClientBuilder;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest()
class LoginTest {

    @Autowired
    WebApplicationContext context;

    WebClient webClient;
    @BeforeEach
    public void setup() {
        webClient = MockMvcWebClientBuilder
                .webAppContextSetup(context, SecurityMockMvcConfigurers.springSecurity())
                .build();

        webClient.getOptions().setJavaScriptEnabled(false);
    }

    @AfterEach
    public void close() {
        webClient.close();
    }

    @Test
    void testInvalidLoginPage() throws IOException {
        HtmlPage loginFormPage = webClient.getPage("http://localhost/");

        HtmlTextInput username = loginFormPage.getHtmlElementById("username");
        HtmlPasswordInput password = loginFormPage.getHtmlElementById("password");

        username.setText("username");
        password.setText("bad password");

        HtmlForm loginForm = loginFormPage.getFormByName("login");
        HtmlButton loginButton = loginForm.getOneHtmlElementByAttribute("button", "type", "submit");

        HtmlPage loginResult = loginButton.click();
        HtmlElement errorDiv = loginResult.getHtmlElementById("error");
        assertEquals("Invalid username or password.", errorDiv.getTextContent().strip());
    }
    @Test
    void testInvalidSecondLoginPageUri() throws IOException {
        HtmlPage loginFormPage = webClient.getPage("http://localhost/login");

        HtmlTextInput username = loginFormPage.getHtmlElementById("username");
        HtmlPasswordInput password = loginFormPage.getHtmlElementById("password");

        username.setText("username");
        password.setText("bad password");

        HtmlForm loginForm = loginFormPage.getFormByName("login");
        HtmlButton loginButton = loginForm.getOneHtmlElementByAttribute("button", "type", "submit");

        HtmlPage loginResult = loginButton.click();
        HtmlElement errorDiv = loginResult.getHtmlElementById("error");
        assertEquals("Invalid username or password.", errorDiv.getTextContent().strip());
    }


}
